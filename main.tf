provider "aws" {
//  alias = "first"
  region = var.aws_region
//  access_key = "AKIA2CI4APNJRMF3LBV4"
//  secret_key = "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"
  access_key = "${var.access_key}"
  secret_key = "${var.secret_key}"
}

resource "aws_vpc" "technoconfig" {
  cidr_block = var.vpc_cidr
  enable_dns_hostnames = true
}

resource "aws_internet_gateway" "technoconfigIGW" {
  vpc_id = aws_vpc.technoconfig.id
}

resource "aws_subnet" "eu-central-1a-public1" {
  cidr_block = var.public_subnet_cidr
  vpc_id = aws_vpc.technoconfig.id
  availability_zone = "eu-central-1a"

  tags = {
    Name = "Public subnet"
  }
}

resource "aws_route_table" "eu-central-1a-public1" {
  vpc_id = aws_vpc.technoconfig.id
  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.technoconfigIGW.id
  }
  tags = {
    Name = "RT-public"
  }
}

resource "aws_route_table_association" "eu-central-1a-public1" {
  subnet_id = aws_subnet.eu-central-1a-public1.id
  route_table_id = aws_route_table.eu-central-1a-public1.id
}

resource "aws_instance" "centos" {
  count = var.instance_count
  ami = "ami-08b6d44b4f6f7b279"
  instance_type = "t3.micro"
  key_name = "aws_ssh"
  vpc_security_group_ids = ["${aws_security_group.web.id}"]
  subnet_id = "${aws_subnet.eu-central-1a-public1.id}"
  associate_public_ip_address = true
  source_dest_check = false

  tags = {
    Name  = "${element(var.instance_tags, count.index)}"
  }

}

resource "aws_instance" "centos1" {
//  count = var.instance_count
  ami = "ami-08b6d44b4f6f7b279"
  instance_type = "t2.medium"
  key_name = "aws_ssh"
  vpc_security_group_ids = ["${aws_security_group.web.id}"]
  subnet_id = "${aws_subnet.eu-central-1a-public1.id}"
  associate_public_ip_address = true
  source_dest_check = false

  tags = {
    Name = "AWX"
//    Name  = "${element(var.instance_tags, count.index)}"
  }

}

resource "aws_security_group" "web" {
  name = "SG_web"
  description = "Allow all http connections"

  ingress {
    from_port = 22
    to_port = 22
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port = 8082
    to_port = 8082
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    from_port = 5432
    to_port = 5432
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    from_port = 8081
    to_port = 8081
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    from_port = 8088
    to_port = 8088
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port = 0
    to_port = 0
    protocol = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port = 80
    to_port = 80
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port = 80
    to_port = 80
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }



//  ingress {
//    from_port = 443
//    to_port = 443
//    protocol = "tcp"
//    cidr_blocks = ["0.0.0.0/0"]
//  }
//
//  ingress {
//    from_port = -1
//    to_port = -1
//    protocol = "icmp"
//    cidr_blocks = ["0.0.0.0/0"]
//  }
//
//  ingress {
//    from_port = 22
//    to_port = 22
//    protocol = "tcp"
//    cidr_blocks =  ["0.0.0.0/0"]
//  }
//  egress {
//    from_port       = 0
//    to_port         = 0
//    protocol        = "-1"
//    cidr_blocks     = ["0.0.0.0/0"]
//  }

  vpc_id = aws_vpc.technoconfig.id


}
output "instance_ips" {
  value = aws_instance.centos.*.public_ip
}
output "instance_ip" {
  value = aws_instance.centos1.*.public_ip
}
